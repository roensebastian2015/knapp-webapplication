<?php

	function getMinAveMaxAmounts(){
		if(srvRequestMethod() == 'GET'){
		
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
		
			$sql = "SELECT MIN(amount), ROUND(AVG(amount::numeric), 0)::money as AVG, MAX(amount) FROM gis_data.tbl_grant";
			
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data = $row;
			
			}
			
			pg_close($link);
			
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
	}
	
	function getTotalByCity(){
		
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT (SELECT loc_name from gis_data.tbl_config_location where loc_id = l.loc_id) as city, sum(g.amount::numeric) as total, (SELECT ST_AsGeoJSON(geometry) from gis_data.tbl_config_location where loc_id = l.loc_id) as geom FROM gis_data.tbl_location as l join gis_data.tbl_grant as g on g.id_grant = l.grant_id group by city, geom order by city asc";
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row;
			
			}
			
			pg_close($link);
			
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
		
		
	}
	
	
	function getSingleLocation(){
		
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			$id = $_GET['id'];
			$sql = "SELECT loc_name, geom, grant_id, grant_recipient FROM gis_data.grant_id_location where grant_id = '$id'";
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row;
			
			}
			
			pg_close($link);
			
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
		
	}
	
	function getListYears(){
		
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT distinct year_funded FROM gis_data.tbl_grant order by year_funded asc";
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row['year_funded'];
			
			}
			
			pg_close($link);
			
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
	}
	
	
	function getTotalsByYears(){
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT  SUM(amount::numeric) AS amount FROM gis_data.tbl_grant GROUP BY year_funded order by year_funded asc";
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = (int) $row['amount'];
			
			}
			
			pg_close($link);
			
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
	}
	
	function getGrantsScatterPoints(){
		
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT con.loc_name, (SELECT g.grant_recipient from gis_data.tbl_grant as g where g.id_grant = loc.grant_id), ST_AsGeoJSon(RandomPointsInPolygon(geometry, 1)) as geom,
(SELECT g.amount::numeric from gis_data.tbl_grant as g where g.id_grant = loc.grant_id), (select json_build_object('program', p.program_name, 'color', p.style_color) from gis_data.tbl_program as p join gis_data.tbl_grant as g on p.program_id = g.program_id where g.id_grant = loc.grant_id)
FROM gis_data.tbl_location as loc 
join gis_data.tbl_config_location as con on loc.loc_id = con.loc_id";
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row;
			
			}
			
			pg_close($link);
			
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
		
	}
	
	
	//GENERATES HEAT MAPS FOR CERTAIN BUCKET...
	function generateHeatMapForXBucket(){
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			$bucket = $_GET['bucket'];
			
			
			$sql = "SELECT amount::numeric,(SELECT ST_AsGeoJSon(RandomPointsInPolygon(geometry, 1)) FROM  gis_data.tbl_config_location where loc_id = l.loc_id) as geom  
			FROM gis_data.tbl_grant as g join gis_data.tbl_location as l on g.id_grant = l.grant_id where g.program_id = '$bucket'";
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row;
			
			}
			
			pg_close($link);
			
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
	}
	

?>