<?php
	
	//MODULE GET SUMMARY REPORT FOR PROGRAMS AND GRANTES WITH SUMS
	function getSummaryReport(){
		
		if (srvRequestMethod() == 'GET') {
		
		 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT prgm.program_id, 'false' as selection, prgm.program_name, json_agg(json_build_object('grantees',g.grant_recipient, 'total', g.amount, 'selection', 'false', 'grant_id', g.id_grant, 'color', prgm.style_color)) AS grants_list, sum(g.amount) as total
FROM gis_data.tbl_program as prgm join gis_data.tbl_grant as g on prgm.program_id = g.program_id
GROUP  BY 1 order by prgm.program_name;";
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row;
			
			}
			
			pg_close($link);
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
	}
	
	//GET LOCATIONS FOR ALL DATA...
	
	function getLocationGrants(){
		if(srvRequestMethod() == 'GET'){
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT (SELECT style_color FROM gis_data.tbl_program where program_id = g.program_id) as color, (SELECT ST_AsGeoJSON(geometry) FROM gis_data.tbl_config_location where loc_id = loc.loc_id) as geom, g.grant_recipient FROM gis_data.tbl_location as loc JOIN gis_data.tbl_grant as g on loc.grant_id = g.id_grant;";
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row;
			
			}
			
			pg_close($link);
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
		
	}
	
	
	function getSingleProgramLocation(){
		
		if(srvRequestMethod() == "GET"){
			$id = $_GET['id'];
			
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "select  loc.loc_id, g.grant_recipient, g.program_id, (select ST_AsGeoJSON(geometry) as geom from gis_data.tbl_config_location as con where con.loc_id = loc.loc_id), (select style_color from gis_data.tbl_program as p where p.program_id = g.program_id) as color from  gis_data.tbl_grant  as g join gis_data.tbl_location as loc  on g.id_grant = loc.grant_id where g.program_id = '$id' group by loc.loc_id, g.grant_recipient, g.program_id";
			
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data [] = $row;
			
			}
			pg_close($link);
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
		
		
		
	}
	
	function getProgramDescription(){
		if(srvRequestMethod() == "GET"){
			$id = $_GET['id'];
			
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT description FROM gis_data.tbl_program where program_id = '$id'";
			
			
			$data = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $data = $row;
			
			}
			pg_close($link);
			return json_encode($data);
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
	}
	
	
	
	
	

?>