<?php
	
	
	function programBarReport(){
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT  prgm.program_name,  sum(g.amount::numeric) as total, prgm.style_color as color FROM gis_data.tbl_program as prgm join gis_data.tbl_grant as g on prgm.program_id = g.program_id GROUP  BY prgm.program_name, prgm.style_color order by prgm.program_name";
			
			$total = array();
			$name  = array();
			$color = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $total [] = $row['total'];
			  $name []= $row['program_name'];
			  $color []= $row['color'];
			
			}
			
			pg_close($link);
			
			return json_encode(array("money" => $total, "name" => $name, "colors" => $color));
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
		
	}
	
	function scheduleJob() {
		
		if(srvRequestMethod() == "GET") {
			
			ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
			$name = $_GET['name'];
			$location = "\\\\911_Dept\\04_SYSTEMS\\06_WEBSERVER\\inetpub\\wwwroot\\knapp_report\\schedule\\";
			$file = $name . ".txt";
			$myfile = fopen($location . $file, "w") or die("Unable to open file!");
		
			fwrite($myfile, $name); //0
			fclose($myfile);
			
			$fileName = "\\\\911_Dept\\04_SYSTEMS\\06_WEBSERVER\\inetpub\\wwwroot\\knapp_report\\" . $name . "\\KnappReport.xlsx";
		
			while (file_exists($fileName) == false) { //stop until file found
			
				
			}
			
			
			sleep(2); //Wait for the document to be generate...
			return json_encode(array("good" => true));
			
			
			
		}
		
	}
	
	
	function programBarByCityReport(){
		if(srvRequestMethod() == "GET"){
			
			 //Link is the connection to the postgres sql virtual machine...
			$link = pg_connect("host=" . IP . " dbname=" . DATABASE . "   user=" . USER) or die("ERROR");
			
			$sql = "SELECT (SELECT loc_name from gis_data.tbl_config_location 
						where loc_id = l.loc_id) as city, sum(g.amount::numeric) as total FROM gis_data.tbl_location as l 
						join gis_data.tbl_grant as g on g.id_grant = l.grant_id group by city order by city asc";
			
			$city  = array();
			$total = array();
			$object = pg_query($link, $sql);
			
			while($row = pg_fetch_assoc($object))
			{
			  $city [] = $row['city'];
			  $total [] = $row['total'];
			
			}
			
			pg_close($link);
			
			return json_encode(array("money" => $total, "name" => $city));
		}else{
			
			return json_encode(array("error" => true, "REASON" => "Method Not Available"));
		}
		
		
	}
?>