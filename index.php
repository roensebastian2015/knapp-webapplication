<?php

  //This is were everything starts
  //We need to force that it doesnt listen to other servers only from the same domain...
	// error_reporting(E_ALL);
	// 		ini_set('display_errors', 1);
	// 		ini_set('display_startup_errors', 1);
	require_once '../spartan/api/controller/inc/srv.php';

 //Define IP for connection of database...
 define('IP', 'spartandb.lrgvdc911.org');

 //Define schema, database...
 define('DATABASE', 'knappccf');
 define('SCHEMA', 'gis_data');

 //Define Tables
 define('TABLE_CONFIG_LOCATION', 'tbl_config_location');
 define('TABLE_GRANT', 'tbl_grant');
 define('PROGRAM', 'tbl_program');
 define('TABLE_LOCATION', 'tbl_location');

 //Define user connection
 define('USER', 'spartan');


 $infos = explode('/', $_SERVER['PATH_INFO']);

 $action = NULL;

// header("Access-Control-Allow-Origin: *");

if (count($infos) > 2) {
	$controller = strtolower($infos[1]);
	$action = strtolower($infos[2]);
  //From here we include what controller


	include("controller/$controller.php");

  //And what funciton to call from that controller..

  if(function_exists($action))
  {

    $answer = call_user_func($action);
    echo $answer;
 }
 else{

	echo "NO ANSWER";
 }
}


?>
